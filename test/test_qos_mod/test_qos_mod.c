/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <unistd.h>
#include <string.h>

#include <amxc/amxc_llist.h>
#include <qosmod/module_getters.h>

#include "qos-assert.h"
#include "test_qos_mod.h"

#ifndef UNUSED
#define UNUSED __attribute__((unused))
#endif

static amxc_var_t* create_variant(void) {
    //create a htable variant just like the one which is parsed from the core
    amxc_var_t* data = NULL;
    amxc_var_new(&data);
    amxc_var_set_type(data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, data, "index", 1);
    amxc_var_t* params = NULL;
    params = amxc_var_add_key(amxc_htable_t, data, PATH, NULL);
    amxc_var_add_key(bool, params, "Enable", true);
    amxc_var_add_key(uint32_t, params, "Precedence", 3);
    amxc_var_add_key(cstring_t, params, "Alias", "test");
    amxc_var_add_key(cstring_t, params, "TrafficClasses", "1, 2, 3");
    amxc_var_add_key(cstring_t, params, "Interface", "testintf");
    amxc_var_add_key(bool, params, "AllInterfaces", true);
    amxc_var_add_key(bool, params, "HardwareAssisted", true);
    amxc_var_add_key(uint32_t, params, "BufferLength", 10);
    amxc_var_add_key(uint32_t, params, "Weight", 50);
    amxc_var_add_key(uint32_t, params, "REDPercentage", 50);
    amxc_var_add_key(cstring_t, params, "SchedulerAlgorithm", "WRR");
    amxc_var_add_key(int32_t, params, "ShapingRate", 500);
    amxc_var_add_key(int32_t, params, "ShapingBurstSize", 350);
    amxc_var_add_key(cstring_t, params, "DefaultQueue", "queue1");
    amxc_var_add_key(int32_t, params, "AssuredRate", 150);
    return data;

}

void test_qos_module_ht_get_enable(UNUSED void** state) {
    amxc_var_t* data = create_variant();
    bool enabled = true;
    assert_int_equal(enabled, qos_module_ht_get_enable(data));
    amxc_var_delete(&data);
}

void test_qos_module_ht_get_precedence(UNUSED void** state) {
    amxc_var_t* data = create_variant();
    uint32_t precedence = 3;
    assert_int_equal(precedence, qos_module_ht_get_precedence(data));
    amxc_var_delete(&data);
}

void test_qos_module_ht_get_alias(UNUSED void** state) {
    amxc_var_t* data = create_variant();
    char* alias = "test";
    assert_string_equal(alias, qos_module_ht_get_alias(data));
    amxc_var_delete(&data);
}

void test_qos_module_ht_get_traffic_classes(UNUSED void** state) {
    amxc_var_t* data = create_variant();
    cstring_t trafficclasses = "1, 2, 3";
    assert_string_equal(trafficclasses, qos_module_ht_get_traffic_classes(data));
    amxc_var_delete(&data);
}

void test_qos_module_ht_get_interface(UNUSED void** state) {
    amxc_var_t* data = create_variant();
    cstring_t interface = "testintf";
    assert_string_equal(interface, qos_module_ht_get_interface(data));
    amxc_var_delete(&data);
}

void test_qos_module_ht_get_all_interfaces(UNUSED void** state) {
    amxc_var_t* data = create_variant();
    bool all_interfaces = true;
    assert_int_equal(all_interfaces, qos_module_ht_get_all_interfaces(data));
    amxc_var_delete(&data);
}

void test_qos_module_ht_get_hardware_assisted(UNUSED void** state) {
    amxc_var_t* data = create_variant();
    bool hardware_assisted = true;
    assert_int_equal(hardware_assisted, qos_module_ht_get_hardware_assisted(data));
    amxc_var_delete(&data);
}

void test_qos_module_ht_get_buffer_length(UNUSED void** state) {
    amxc_var_t* data = create_variant();
    uint32_t buffer_length = 10;
    assert_int_equal(buffer_length, qos_module_ht_get_buffer_length(data));
    amxc_var_delete(&data);
}

void test_qos_module_ht_get_weight(UNUSED void** state) {
    amxc_var_t* data = create_variant();
    uint32_t weight = 50;
    assert_int_equal(weight, qos_module_ht_get_weight(data));
    amxc_var_delete(&data);
}

void test_qos_module_ht_get_red_percentage(UNUSED void** state) {
    amxc_var_t* data = create_variant();
    uint32_t red_percentage = 50;
    assert_int_equal(red_percentage, qos_module_ht_get_red_percentage(data));
    amxc_var_delete(&data);
}

void test_qos_module_ht_get_scheduler_algorithm(UNUSED void** state) {
    amxc_var_t* data = create_variant();
    cstring_t scheduler_algorithm = "WRR";
    assert_string_equal(scheduler_algorithm, qos_module_ht_get_scheduler_algorithm(data));
    amxc_var_delete(&data);
}

void test_qos_module_ht_get_shaping_rate(UNUSED void** state) {
    amxc_var_t* data = create_variant();
    int32_t shaping_rate = 500;
    assert_int_equal(shaping_rate, qos_module_ht_get_shaping_rate(data));
    amxc_var_delete(&data);
}

void test_qos_module_ht_get_shaping_burst_size(UNUSED void** state) {
    amxc_var_t* data = create_variant();
    int32_t shaping_burst_size = 350;
    assert_int_equal(shaping_burst_size, qos_module_ht_get_shaping_burst_size(data));
    amxc_var_delete(&data);
}

void test_qos_module_ht_get_default_queue(UNUSED void** state) {
    amxc_var_t* data = create_variant();
    cstring_t default_queue = "queue1";
    assert_string_equal(default_queue, qos_module_ht_get_default_queue(data));
    amxc_var_delete(&data);
}

void test_qos_module_ht_get_assured_rate(UNUSED void** state) {
    amxc_var_t* data = create_variant();
    int32_t assured_rate = 150;
    assert_int_equal(assured_rate, qos_module_ht_get_assured_rate(data));
    amxc_var_delete(&data);
}

void test_qos_module_ht_get_index(UNUSED void** state) {
    amxc_var_t* data = create_variant();
    uint32_t index = 1;
    assert_int_equal(index, qos_module_ht_get_index(data));
    amxc_var_delete(&data);
}
