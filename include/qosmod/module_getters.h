/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__MOD_GETTERS_H__)
#define __MOD_GETTERS_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <amxc/amxc.h>
#define PATH "parameters"

typedef enum _qos_status {
    QOS_STATUS_DISABLED,
    QOS_STATUS_ENABLED,
    QOS_STATUS_ERROR_MISCONFIGURED,
    QOS_STATUS_ERROR,
    QOS_STATUS_LAST
} qos_status_t;

qos_status_t qos_module_ht_get_status(amxc_var_t* args);
static inline uint32_t qos_module_ht_get_index(amxc_var_t* args) {
    return GETP_UINT32(args, "index");
}

static inline bool qos_module_ht_get_enable(amxc_var_t* args) {
    return GETP_BOOL(args, PATH ".Enable");
}
static inline uint32_t qos_module_ht_get_precedence(amxc_var_t* args) {
    return GETP_UINT32(args, PATH ".Precedence");
}
static inline const char* qos_module_ht_get_alias(amxc_var_t* args) {
    return GETP_CHAR(args, PATH ".Alias");
}
static inline const char* qos_module_ht_get_traffic_classes(amxc_var_t* args) {
    return GETP_CHAR(args, PATH ".TrafficClasses");
}
static inline const char* qos_module_ht_get_interface(amxc_var_t* args) {
    return GETP_CHAR(args, PATH ".Interface");
}
static inline bool qos_module_ht_get_all_interfaces(amxc_var_t* args) {
    return GETP_BOOL(args, PATH ".AllInterfaces");
}
static inline bool qos_module_ht_get_hardware_assisted(amxc_var_t* args) {
    return GETP_BOOL(args, PATH ".HardwareAssisted");
}
static inline uint32_t qos_module_ht_get_buffer_length(amxc_var_t* args) {
    return GETP_UINT32(args, PATH ".BufferLength");
}
static inline uint32_t qos_module_ht_get_weight(amxc_var_t* args) {
    return GETP_UINT32(args, PATH ".Weight");
}
static inline uint32_t qos_module_ht_get_red_threshold(amxc_var_t* args) {
    return GETP_UINT32(args, PATH ".REDThreshold");
}
static inline uint32_t qos_module_ht_get_red_percentage(amxc_var_t* args) {
    return GETP_UINT32(args, PATH ".REDPercentage");
}
static inline const char* qos_module_ht_get_scheduler_algorithm(amxc_var_t* args) {
    return GETP_CHAR(args, PATH ".SchedulerAlgorithm");
}
static inline int32_t qos_module_ht_get_shaping_rate(amxc_var_t* args) {
    return GETP_INT32(args, PATH ".ShapingRate");
}
static inline uint32_t qos_module_ht_get_shaping_burst_size(amxc_var_t* args) {
    return GETP_UINT32(args, PATH ".ShapingBurstSize");
}

static inline const char* qos_module_ht_get_default_queue(amxc_var_t* args) {
    return GETP_CHAR(args, PATH ".DefaultQueue");
}

static inline int32_t qos_module_ht_get_assured_rate(amxc_var_t* args) {
    return GETP_INT32(args, PATH ".AssuredRate");
}

static inline const char* qos_module_ht_get_drop_algorithm(amxc_var_t* args) {
    return GETP_CHAR(args, PATH ".DropAlgorithm");
}

static inline uint32_t qos_module_ht_get_queue_key(amxc_var_t* args) {
    return GETP_UINT32(args, PATH ".QueueKey");
}

#ifdef __cplusplus
}
#endif

#endif // __MOD_GETTERS_H__
