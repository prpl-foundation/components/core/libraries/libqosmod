# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.1.6 - 2022-05-19(12:46:43 +0000)

### Fixes

- [Gitlab CI][Unit tests][valgrind] Pipeline doesn't stop when memory leaks are detected

## Release v0.1.5 - 2022-02-21(08:04:09 +0000)

### Other

- Opensource library

## Release v0.1.4 - 2022-02-11(15:16:51 +0000)

### Changes

- - [mod-qos-tc] create working linux tc module

## Release v0.1.3 - 2022-01-28(14:59:49 +0000)

## Release v0.1.2 - 2022-01-28(11:19:13 +0000)

### Other

- Added QueueKey parameter to lib qosmod

## Release v0.1.1 - 2022-01-03(15:21:50 +0000)

### Changes

- Use BSD-2-Clause-Patent license

## Release v0.1.0 - 2021-12-16(08:26:52 +0000)

